#include <jni.h>
#include <EGL/egl.h>
#include <GLES2/gl2ext.h>
#include <android_native_app_glue.h>
#include <unistd.h>
#include <iostream>

#include "Log.h"
#include "Thread.h"
#include "renderer/Shader.h"
#include "math/Vector3.h"
#include "renderer/GLExtensionHelper.h"
#include "resource/PNGLoader.h"
#include "renderer/GLRenderer.h"
#include "resource/Sound.h"

struct app_state
{
	struct android_app* app;
	int32_t width;
	int32_t height;

	EGLDisplay display;
	EGLSurface surface;
	EGLContext context;
	chs::GLRenderer renderer;
};

static int init_display(struct app_state* state)
{
	const EGLint attribs[] = {
			EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
			EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
			EGL_BLUE_SIZE, 8,
			EGL_GREEN_SIZE, 8,
			EGL_RED_SIZE, 8,
			EGL_NONE
	};

	EGLint format, w, h;
	EGLConfig config;
	EGLint numConfigs;

	state->display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	eglInitialize(state->display, 0, 0);
	eglChooseConfig(state->display, attribs, &config, 1, &numConfigs);
	eglGetConfigAttrib(state->display, config, EGL_NATIVE_VISUAL_ID, &format);

	ANativeWindow_setBuffersGeometry(state->app->window, 0, 0, format);

	state->surface = eglCreateWindowSurface(state->display, config, state->app->window, NULL);

	EGLint AttribList[] =
	{
	    EGL_CONTEXT_CLIENT_VERSION, 2,
	    EGL_NONE
	};

	state->context = eglCreateContext(state->display, config, NULL, AttribList);

	if (eglMakeCurrent(state->display, state->surface, state->surface, state->context) == EGL_FALSE)
	{
		LOG_E("Unable to eglMakeCurrent");
		return -1;
	}

	eglQuerySurface(state->display, state->surface, EGL_WIDTH, &w);
	eglQuerySurface(state->display, state->surface, EGL_HEIGHT, &h);

	glViewport(0, 0, w, h);

	LOG_I("Width: %i Height: %i", w, h);

	return 0;
}

static void terminate_display(struct app_state* state)
{
	if (state->display != EGL_NO_DISPLAY)
	{
		eglMakeCurrent(state->display, EGL_NO_DISPLAY, EGL_NO_SURFACE, EGL_NO_SURFACE);

		if (state->context != EGL_NO_CONTEXT)
		{
			eglDestroyContext(state->display, state->context);
		}

		if (state->surface != EGL_NO_SURFACE)
		{
			eglDestroySurface(state->display, state->surface);
		}

		eglTerminate(state->display);
	}

	state->display = EGL_NO_DISPLAY;
	state->context = EGL_NO_CONTEXT;
	state->surface = EGL_NO_SURFACE;
}

static void app_handle_cmd(struct android_app* app, int32_t cmd)
{
	struct app_state* state = (struct app_state*)app->userData;

	switch (cmd)
	{
		case APP_CMD_INIT_WINDOW:
			if (state->app->window != NULL)
			{
				init_display(state);

				state->renderer.onCreate();
				state->renderer.onDrawFrame();
				eglSwapBuffers(state->display, state->surface);
			}
			break;
		case APP_CMD_TERM_WINDOW:
			terminate_display(state);
			break;
	}
}

static int32_t app_input_handle(struct android_app* app, AInputEvent* event)
{
	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION)
	{
		size_t pointerCount = AMotionEvent_getPointerCount(event);

		for (size_t i = 0; i < pointerCount; ++i)
		{
			LOG_I("Received motion event from pointer %zu: (%.2f, %.2f)", i, AMotionEvent_getX(event, i), AMotionEvent_getY(event, i));
		}
		return 1;
	}

	return 0;
}

void android_main(struct android_app* state)
{
	struct app_state app_state;
	app_dummy();

	sleep(5);

	memset(&app_state, 0, sizeof(app_state));

	state->userData = &app_state;
	state->onAppCmd = app_handle_cmd;
	state->onInputEvent = app_input_handle;
	app_state.app = state;

	app_state.renderer.setAndroidApp(state);

	chs::Sound sound;
	sound.init(2, 48000);
	sound.openFile(state, "sound/bugsbunny1.wav");

	while(1)
	{
		int ident;
		int events;
		struct android_poll_source* source;

		if((ident = ALooper_pollAll(0, NULL, &events, (void**) &source)) >= 0)
		{
			if (source != NULL)
			{
				source->process(state, source);
			}


			if (state->destroyRequested != 0)
			{
				terminate_display(&app_state);
				return;
			}
		}

		//sound.audioOut();

		//app_state.renderer.onDrawFrame();

		//eglSwapBuffers(app_state.display, app_state.surface);
	}
}
