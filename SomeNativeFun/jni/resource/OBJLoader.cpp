/*
 * OBJLoader.cpp
 *
 *  Created on: 06.12.2014
 *      Author: DerRM
 */

#include <resource/OBJLoader.h>
#include "Log.h"

namespace chs {

OBJLoader::OBJLoader()
{
}

Mesh OBJLoader::loadMesh(android_app* pApplication, const char* pPath)
{
	std::string objString(readObjString(m_ResourceManager.openFile(pApplication, pPath)));
	std::stringstream stringStream(objString);

	std::string line;
	std::vector<std::string> lineList;

	while (!safeGetline(stringStream, line).eof())
	{
		std::stringstream lineStream(line);
		std::string token;
		std::vector<std::string> tokenList;

		while(std::getline(lineStream, token, ' '))
		{
			tokenList.push_back(token);
		}

		if (tokenList.size() == 4)
		{
			if (tokenList[0].compare("v") == 0)
			{
				LOG_I("vertex: x: %s y: %s z: %s", tokenList[1].c_str(), tokenList[2].c_str(), tokenList[3].c_str());
			}
			else if (tokenList[0].compare("vn") == 0)
			{
				LOG_I("vertex normal: x: %s y: %s z: %s", tokenList[1].c_str(), tokenList[2].c_str(), tokenList[3].c_str());
			}
			else if (tokenList[0].compare("f") == 0)
			{
				LOG_I("face: idx1: %s idx2: %s idx3: %s", tokenList[1].c_str(), tokenList[2].c_str(), tokenList[3].c_str());
			}
		}
		tokenList.clear();
	}

	m_ResourceManager.closeFile();

	return (Mesh) {0, NULL};
}

std::istream& OBJLoader::safeGetline(std::istream& is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }

    return is;
}

char* OBJLoader::readObjString(AAsset* pAsset)
{
	size_t size = AAsset_getLength(pAsset);
    char* buffer = (char*)malloc (sizeof(char)*size + 1);
	AAsset_read(pAsset, buffer, size);
	buffer[size] = 0;
	return buffer;
}

} /* namespace chs */
