/*
 * ResourceManager.h
 *
 *  Created on: 24.08.2014
 *      Author: DerRM
 */

#ifndef RESOURCEMANAGER_H_
#define RESOURCEMANAGER_H_

#include <android_native_app_glue.h>

namespace chs
{

class ResourceManager
{
public:
	ResourceManager();
	~ResourceManager();

	const char* getPath() { return m_pPath; }
	AAsset* openFile(android_app* pApplication, const char* pPath);
	void closeFile();
protected:
	const char* m_pPath;
	AAsset* m_pAsset;
};

} /* namespace chs */

#endif /* RESOURCEMANAGER_H_ */
