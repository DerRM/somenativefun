/*
 * Sound.cpp
 *
 *  Created on: 11.01.2015
 *      Author: DerRM
 */

#include "resource/Sound.h"
#include "Log.h"

namespace chs {

Sound::Sound()
: m_audioEngine(0),
  m_audioEngineObject(0),
  m_outputMixerObject(0),
  m_audioPlayer(0),
  m_audioPlayerObject(0),
  m_playerBufferQueue(0),
  m_audioVolume(0),
  m_pAsset(0),
  m_audioBufferSize(47315),
  m_audioReadyToPlay(false)
{
	// TODO Auto-generated constructor stub
}

bool Sound::init(SLuint32 numChannels, SLuint32 samplesPerSec)
{
	m_outputBuffer[0] = (SLint16*)calloc(numChannels * 1024, sizeof(SLint16));
	m_outputBuffer[1] = (SLint16*)calloc(numChannels * 1024, sizeof(SLint16));

	SLresult result = slCreateEngine(&m_audioEngineObject, 0, 0, 0, 0, 0);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with slCreateEngine %d", result);
		return false;
	}

	result = (*m_audioEngineObject)->Realize(m_audioEngineObject, SL_BOOLEAN_FALSE);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with AudioEngineObject::Realize %d", result);
		return false;
	}

	result = (*m_audioEngineObject)->GetInterface(m_audioEngineObject, SL_IID_ENGINE, &m_audioEngine);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with AudioEngineObject::GetInterface %d", result);
		return false;
	}

	LOG_I("AudioEngine initialized");

	result = (*m_audioEngine)->CreateOutputMix(m_audioEngine, &m_outputMixerObject, 0, 0, 0);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with AudioEngine::CreateOutputMix %d", result);
		return false;
	}

	result = (*m_outputMixerObject)->Realize(m_outputMixerObject, SL_BOOLEAN_FALSE);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with OutputMixer::Realize %d", result);
		return false;
	}

	SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {};
	loc_bufq.locatorType = SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE;
	loc_bufq.numBuffers = 2;

	SLDataFormat_PCM format_pcm = {};
	format_pcm.formatType = SL_DATAFORMAT_PCM;
	format_pcm.numChannels = 2;
	format_pcm.samplesPerSec = SL_SAMPLINGRATE_44_1;
	format_pcm.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
	format_pcm.containerSize = SL_PCMSAMPLEFORMAT_FIXED_16;
	format_pcm.channelMask = SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT;
	format_pcm.endianness = SL_BYTEORDER_LITTLEENDIAN;

	SLDataSource audioSrc = {};
	audioSrc.pLocator = &loc_bufq;
	audioSrc.pFormat = &format_pcm;

	SLDataLocator_OutputMix loc_outmix = {};
	loc_outmix.locatorType = SL_DATALOCATOR_OUTPUTMIX;
	loc_outmix.outputMix = m_outputMixerObject;

	SLDataSink_ audioSnk = {};
	audioSnk.pLocator = &loc_outmix;
	audioSnk.pFormat = 0;

	const SLInterfaceID ids1[] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE/*, SL_IID_VOLUME*/};
	const SLboolean req1[] = {SL_BOOLEAN_TRUE/*, SL_BOOLEAN_TRUE*/};

	result = (*m_audioEngine)->CreateAudioPlayer(m_audioEngine, &m_audioPlayerObject, &audioSrc, &audioSnk, 1, ids1, req1);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with AudioEnigne::CreateAudioPlayer %d", result);
		return false;
	}

	result = (*m_audioPlayerObject)->Realize(m_audioPlayerObject, SL_BOOLEAN_FALSE);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with AudioPlayerObject::Realize %d", result);
		return false;
	}

	result = (*m_audioPlayerObject)->GetInterface(m_audioPlayerObject, SL_IID_PLAY, &m_audioPlayer);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with AudioPlayerObject::GetInterface getting audio player %d", result);
		return false;
	}

	result = (*m_audioPlayerObject)->GetInterface(m_audioPlayerObject, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, &m_playerBufferQueue);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with AudioPlayerObject::GetInterface getting buffer queue %d", result);
		return false;
	}
/*
	result = (*m_audioPlayerObject)->GetInterface(m_audioPlayerObject, SL_IID_VOLUME, &m_audioVolume);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with AudioPlayerObject::GetInterface getting volume %d", result);
		return false;
	}
*/
	result = (*m_playerBufferQueue)->RegisterCallback(m_playerBufferQueue, Sound::PlayerCallback, this);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with PlayerBufferQueue::RegisterCallback %d", result);
		return false;
	}

	audioOut();

	result = (*m_audioPlayer)->SetPlayState(m_audioPlayer, SL_PLAYSTATE_PLAYING);

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("error with AudioPlayer::SetPlayState %d", result);
		return false;
	}

	return true;
}

void Sound::audioOut()
{
	static double time = 0;
	static SLint32 currentBuffer = 0;
	SLuint32 bufferSize = 1024;

	SLint16* outputBuffer = m_outputBuffer[currentBuffer];

	if (m_audioReadyToPlay)
	{
		static int audioBufferSize = m_wavFile.dataChunkSize;

		static SLuint8* inputBuffer = (SLuint8*)m_wavFile.dataSampleData;

		for (int bufferIndex = 0; bufferIndex < 256; ++bufferIndex)
		{
			SLint16 value = ((((SLint16)*inputBuffer++) * 256) - 32768);

			*outputBuffer++ = value;
			*outputBuffer++ = value;
			*outputBuffer++ = value;
			*outputBuffer++ = value;
			*outputBuffer++ = value;
			*outputBuffer++ = value;
			*outputBuffer++ = value;
			*outputBuffer++ = value;
		}


		if (audioBufferSize == 0)
		{
			m_audioReadyToPlay = false;
		}

		if (audioBufferSize >= 256)
		{
			audioBufferSize -= 256;
		}
		else
		{
			audioBufferSize = 0;
		}
	}
	else
	{
		for (size_t i = 0; i < bufferSize; ++i)
		{
			SLint16 value = (SLint16)((sin(time)) * 32768);
			*outputBuffer++ = value;
			*outputBuffer++ = value;

			time += 2.0 * M_PI * 440.0f / 44100;
		}
	}

	SLresult result = (*m_playerBufferQueue)->Enqueue(m_playerBufferQueue, m_outputBuffer[currentBuffer], bufferSize * 2 * sizeof(int16_t));

	if (result != SL_RESULT_SUCCESS)
	{
		LOG_E("something is wrong with the buffer %d", result);
	}

	currentBuffer = (currentBuffer + 1) % 2;
}

void Sound::openFile(android_app* pApp, const char* pPath)
{
	m_pAsset = AAssetManager_open(pApp->activity->assetManager, pPath, AASSET_MODE_UNKNOWN);

	parseSoundFile();
}

void Sound::parseSoundFile()
{
	if (!m_pAsset) return;

	size_t size = AAsset_getLength(m_pAsset);
    char* buffer = (char*)malloc (sizeof(char)*size + 1);
	AAsset_read(m_pAsset, buffer, size);

	//m_wavFile.riff = buffer;
	m_wavFile.riffSize = (int32_t)*((int32_t*)(buffer + 4));
	m_wavFile.fmtSize = (int32_t)*((int32_t*)(buffer + 16));
	m_wavFile.fmtCompressionCode = (int16_t)*((int16_t*)(buffer + 20));
	m_wavFile.fmtNumberChannels = (int16_t)*((int16_t*)(buffer + 22));
	m_wavFile.fmtSampleRate = (int32_t)*((int32_t*)(buffer + 24));
	m_wavFile.fmtAverageBytesPerSecond = (int32_t)*((int32_t*)(buffer + 28));
	m_wavFile.fmtBlockAlign = (int16_t)*((int16_t*)(buffer + 32));
	m_wavFile.fmtSigBitsPerSample = (int16_t)*((int16_t*)(buffer + 34));
	m_wavFile.dataChunkSize = (int32_t)*((int32_t*)(buffer + 40));
	m_wavFile.dataSampleData = (void*)(buffer + 44);

	LOG_I("RiffSize: %d, FormatSize: %d, CompressionCode: %d, NumberChannels: %d, SampleRate: %d\n"
			"AverageBytesPerSecond: %d, BlockAlign: %d, SignificantBitsPerSample: %d\n"
			"DataChunkSize: %d", m_wavFile.riffSize, m_wavFile.fmtSize, m_wavFile.fmtCompressionCode, m_wavFile.fmtNumberChannels,
			m_wavFile.fmtSampleRate, m_wavFile.fmtAverageBytesPerSecond, m_wavFile.fmtBlockAlign, m_wavFile.fmtSigBitsPerSample, m_wavFile.dataChunkSize);

	m_audioReadyToPlay = true;
}

void Sound::PlayerCallback(SLAndroidSimpleBufferQueueItf caller, void *pContext)
{
	//LOG_I("PlayerCallback called");
	Sound* sound = (Sound*)pContext;
	sound->audioOut();
}

} /* namespace chs */
