/*
 * OBJLoader.h
 *
 *  Created on: 06.12.2014
 *      Author: DerRM
 */

#ifndef OBJLOADER_H_
#define OBJLOADER_H_

#include <android_native_app_glue.h>
#include <string>
#include <sstream>
#include <vector>
#include "resource/ResourceManager.h"

namespace chs {

typedef struct {
	const float x;
	const float y;
	const float z;
} Vertex;

typedef struct {
	const float u;
	const float v;
} TextureCoordinate;

typedef struct {
	const float x;
	const float y;
	const float z;
} Normal;

typedef struct {
	const size_t vertexCount;
	const Vertex* vertices;
	const size_t texCoordCount;
	const TextureCoordinate* texCoords;
	const size_t normalCount;
	const Normal* normals;
} Face;

typedef struct {
	const size_t faceCount;
	const Face* faces;
} Mesh;

class OBJLoader {
public:
	OBJLoader();
	Mesh loadMesh(android_app* pApplication, const char* pPath);
private:
	ResourceManager m_ResourceManager;
	char* readObjString(AAsset* pAsset);
	std::istream& safeGetline(std::istream& is, std::string& t);
};

} /* namespace chs */

#endif /* OBJLOADER_H_ */
