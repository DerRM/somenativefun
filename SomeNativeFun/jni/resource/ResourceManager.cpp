/*
 * ResourceManager.cpp
 *
 *  Created on: 24.08.2014
 *      Author: DerRM
 */
#include "ResourceManager.h"
#include "Log.h"
#include  <png.h>

namespace chs
{

ResourceManager::ResourceManager()
	: m_pPath(NULL),
	  m_pAsset(NULL)
{

}

ResourceManager::~ResourceManager()
{
}

AAsset* ResourceManager::openFile(android_app* pApplication, const char* pPath)
{
	m_pAsset = AAssetManager_open(pApplication->activity->assetManager, pPath, AASSET_MODE_UNKNOWN);
	return m_pAsset;
}

void ResourceManager::closeFile()
{
	if (m_pAsset != NULL)
	{
		AAsset_close(m_pAsset);
		m_pAsset = NULL;
	}
}

} /* namespace chs */
