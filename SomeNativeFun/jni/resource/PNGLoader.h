/*
 * PNGLoader.h
 *
 *  Created on: 01.12.2014
 *      Author: DerRM
 */

#ifndef PNGLOADER_H_
#define PNGLOADER_H_

#include "ResourceManager.h"
#include <png.h>

namespace chs {

typedef struct
{
	const png_byte* data;
	const png_size_t size;
} DataHandle;

typedef struct
{
	const DataHandle data;
	png_size_t offset;
} ReadDataHandle;

typedef struct
{
	const png_uint_32 width;
	const png_uint_32 height;
	const png_int_32 color_type;
} PngInfo;

typedef struct
{
	const png_uint_32 width;
	const png_uint_32 height;
	const png_size_t size;
	const png_byte* data;
} RawImageData;

class PNGLoader
{
public:
	PNGLoader(android_app* pApplication, const char* pPath);
	~PNGLoader();
	RawImageData getRawImageData();
private:
	AAsset* m_pAsset;
	ResourceManager m_ResourceManager;
};

void read_png_data_callback(png_structp png_ptr, png_byte* png_data, png_size_t read_length);
PngInfo read_and_update_info(const png_structp png_ptr, const png_infop info_ptr);
DataHandle read_entire_png_image(const png_structp png_ptr, const png_infop info_ptr, const png_uint_32 height);

} /* namespace chs */

#endif /* PNGLOADER_H_ */
