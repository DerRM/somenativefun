/*
 * Sound.h
 *
 *  Created on: 11.01.2015
 *      Author: DerRM
 */

#ifndef SOUND_H_
#define SOUND_H_

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <pthread.h>
#include <cmath>
#include <android_native_app_glue.h>

namespace chs {

typedef struct
{
	char riff[4];
	int32_t riffSize;
	char riffType[4];
	char fmt[4];
	int32_t fmtSize;
	int16_t fmtCompressionCode;
	int16_t fmtNumberChannels;
	int32_t fmtSampleRate;
	int32_t fmtAverageBytesPerSecond;
	int16_t fmtBlockAlign;
	int16_t fmtSigBitsPerSample;
	char data[4];
	int32_t dataChunkSize;
	void* dataSampleData;
} WavFile;

class Sound
{
public:
	Sound();

	bool init(SLuint32 numChannels, SLuint32 samplesPerSec);

	void audioOut();

	void openFile(android_app* pApplication, const char* pPath);

private:
	SLEngineItf m_audioEngine;
	SLObjectItf m_audioEngineObject;
	SLObjectItf m_outputMixerObject;
	SLPlayItf m_audioPlayer;
	SLObjectItf m_audioPlayerObject;
	SLAndroidSimpleBufferQueueItf m_playerBufferQueue;
	SLVolumeItf m_audioVolume;

	SLint16* m_outputBuffer[2];

	AAsset* m_pAsset;

	void parseSoundFile();

	WavFile m_wavFile;
	SLuint32 m_audioBufferSize;
	SLboolean m_audioReadyToPlay;

	static void PlayerCallback(SLAndroidSimpleBufferQueueItf caller, void *pContext);
};

} /* namespace chs */

#endif /* SOUND_H_ */
