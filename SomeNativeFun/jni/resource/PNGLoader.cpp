/*
 * PNGLoader.cpp
 *
 *  Created on: 01.12.2014
 *      Author: DerRM
 */

#include "PNGLoader.h"

namespace chs {

PNGLoader::PNGLoader(android_app* pApplication, const char* pPath)
{
	m_pAsset = m_ResourceManager.openFile(pApplication, pPath);
}

PNGLoader::~PNGLoader()
{
	m_ResourceManager.closeFile();
}

RawImageData PNGLoader::getRawImageData()
{
	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if (!png_ptr)
	{
		//return 0;
	}

	png_infop info_ptr = png_create_info_struct(png_ptr);

	if (!info_ptr)
	{
		png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
		//return 0;
	}

	png_size_t png_length = AAsset_getLength(m_pAsset);
	const void* png_data = AAsset_getBuffer(m_pAsset);

	ReadDataHandle png_data_handle = (ReadDataHandle){{(const png_byte*)png_data, png_length}, 0};

	png_set_read_fn(png_ptr, &png_data_handle, read_png_data_callback);

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		//return 0;
	}

	const PngInfo png_info = read_and_update_info(png_ptr, info_ptr);
	const DataHandle raw_image = read_entire_png_image(png_ptr, info_ptr, png_info.height);

	png_read_end(png_ptr, info_ptr);
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

	return (RawImageData) { png_info.width, png_info.height, raw_image.size, raw_image.data };
}

void read_png_data_callback(png_structp png_ptr, png_byte* png_data, png_size_t read_length)
{
	ReadDataHandle* read_data_ptr = (ReadDataHandle*)png_get_io_ptr(png_ptr);
	const png_byte* png_src = read_data_ptr->data.data + read_data_ptr->offset;

	memcpy(png_data, png_src, read_length);
	read_data_ptr->offset += read_length;
}

PngInfo read_and_update_info(const png_structp png_ptr, const png_infop info_ptr)
{
	png_uint_32 width, height;
	png_int_32 bit_depth, color_type;

	png_read_info(png_ptr, info_ptr);
	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, NULL, NULL, NULL);

	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
	{
		png_set_tRNS_to_alpha(png_ptr);
	}

	if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
	{
		png_set_expand_gray_1_2_4_to_8(png_ptr);
	}

	if (color_type == PNG_COLOR_TYPE_PALETTE)
	{
		png_set_palette_to_rgb(png_ptr);
	}

	if (color_type == PNG_COLOR_TYPE_PALETTE || color_type == PNG_COLOR_TYPE_RGB)
	{
		png_set_add_alpha(png_ptr, 0xFF, PNG_FILLER_AFTER);
	}

	if (bit_depth < 8)
	{
		png_set_packing(png_ptr);
	}
	else if (bit_depth == 16)
	{
		png_set_scale_16(png_ptr);
	}

	png_read_update_info(png_ptr, info_ptr);

	color_type = png_get_color_type(png_ptr, info_ptr);

	return (PngInfo) {width, height, color_type};
}

DataHandle read_entire_png_image(const png_structp png_ptr, const png_infop info_ptr, const png_uint_32 height)
{
	const png_size_t row_size = png_get_rowbytes(png_ptr, info_ptr);
	const png_size_t data_length = row_size * height;

	png_byte* raw_image = (png_byte*)malloc(data_length);
	png_byte* row_ptrs[height];

	png_uint_32 i;
	for (i = 0; i < height; ++i)
	{
		row_ptrs[(height - 1) - i] = raw_image + i * row_size;
	}

	png_read_image(png_ptr, &row_ptrs[0]);

	return (DataHandle) {raw_image, data_length};
}

} /* namespace chs */
