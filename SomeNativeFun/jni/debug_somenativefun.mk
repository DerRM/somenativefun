LOCAL_PATH := $(call my-dir)

PROJECT_ROOT_PATH:= $(LOCAL_PATH)/..

include $(CLEAR_VARS)

LS_CPP=$(subst $(1)/,,$(filter-out  $(wildcard $(1)/$(2)*_test.cpp), $(wildcard $(1)/$(2)*.cpp)))
APP_PLATFORM			:= android-10
LOCAL_MODULE    		:= SomeNativeFun
LOCAL_SRC_FILES 		:= $(call LS_CPP,$(LOCAL_PATH),) \
						   $(call LS_CPP,$(LOCAL_PATH),math/) \
						   $(call LS_CPP,$(LOCAL_PATH),renderer/) \
						   $(call LS_CPP,$(LOCAL_PATH),resource/) \
						   add_neon.s cross_neon.s
LOCAL_CFLAGS			:= -Wall -mfloat-abi=softfp -mfpu=neon -march=armv7-a
LOCAL_LDLIBS 			:= -llog -landroid -lEGL -lGLESv2 -lOpenSLES
LOCAL_STATIC_LIBRARIES 	:= android_native_app_glue png

include $(BUILD_SHARED_LIBRARY)

ifdef ENABLE_TEST

include $(CLEAR_VARS)

LS_TEST_CPP=$(subst $(1)/,,$(wildcard $(1)/$(2)*_test.cpp))
APP_PLATFORM 			:= android-10
LOCAL_MODULE			:= SomeNativeFun_unittest
LOCAL_SRC_FILES			:= $(call LS_TEST_CPP,$(LOCAL_PATH),math/)
LOCAL_STATIC_LIBRARIES	:= googletest_main
LOCAL_SHARED_LIBRARIES  := SomeNativeFun

include $(BUILD_EXECUTABLE)

endif

$(call import-add-path, $(PROJECT_ROOT_PATH))
$(call import-module, android/native_app_glue)
$(call import-module, third_party/png)

ifdef ENABLE_TEST

$(call import-module, third_party/googletest)
	
all:POST_BUILD_EVENT
POST_BUILD_EVENT:
ifeq ($(OS),Windows_NT)
	test.bat
else
	$(PROJECT_ROOT_PATH)/test.sh
endif

endif