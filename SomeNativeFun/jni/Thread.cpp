/*
 * Thread.cpp
 *
 *  Created on: 15.06.2014
 *      Author: DerRM
 */

#include "Thread.h"
#include "math/Matrix4x4.h"

namespace chs
{

CThread::CThread()
{
	// TODO Auto-generated constructor stub
}

CThread::~CThread()
{
	// TODO Auto-generated destructor stub
}

void* CThread::runByThread(void* args)
{
	int i;
	int threadNum = (int)args;

	for (i = 0; i < 100; ++i)
	{
		sleep(1);
		LOG_I("thread %d: %d", threadNum, i);

		Matrix4x4 matrix = Matrix4x4::identity();
	}

	if (1 == threadNum)
	{
		int ret = 100;
		return (void*)ret;
	}
	else if (2 == threadNum)
	{
		int ret = 200;
		pthread_exit((void*)ret);
	}

	return 0;
}

void CThread::startThreads()
{
	pthread_t th1, th2;
	int threadNumber1 = 1, threadNumber2 = 2;

	pthread_create(&th1, 0, &runByThread, (void*)threadNumber1);
	pthread_create(&th2, 0, &runByThread, (void*)threadNumber2);
}

}
