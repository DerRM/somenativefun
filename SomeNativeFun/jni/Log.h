/*
 * Log.h
 *
 *  Created on: 15.06.2014
 *      Author: DerRM
 */

#ifndef LOG_H_
#define LOG_H_

#include <android/log.h>

#define LOG_W(...) ((void)__android_log_print(ANDROID_LOG_WARN, "SomeNativeFun", __VA_ARGS__))
#define LOG_E(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "SomeNativeFun", __VA_ARGS__))
#define LOG_I(...) ((void)__android_log_print(ANDROID_LOG_INFO, "SomeNativeFun", __VA_ARGS__))

#endif /* LOG_H_ */
