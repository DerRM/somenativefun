LOCAL_PATH := $(call my-dir)

PROJECT_ROOT_PATH:= $(LOCAL_PATH)/..

include $(CLEAR_VARS)

LS_CPP=$(subst $(1)/,,$(filter-out  $(wildcard $(1)/$(2)*_test.cpp), $(wildcard $(1)/$(2)*.cpp)))
APP_PLATFORM			:= android-10
LOCAL_MODULE    		:= SomeNativeFun
LOCAL_SRC_FILES 		:= $(call LS_CPP,$(LOCAL_PATH),) \
						   $(call LS_CPP,$(LOCAL_PATH),math/) \
						   $(call LS_CPP,$(LOCAL_PATH),renderer/) \
						   $(call LS_CPP,$(LOCAL_PATH),resource/) \
						   add_neon.s cross_neon.s
LOCAL_CFLAGS			:= -Wall -mfloat-abi=softfp -mfpu=neon -march=armv7-a -ffast-math -O2
LOCAL_LDLIBS 			:= -llog -landroid -lEGL -lGLESv2
LOCAL_STATIC_LIBRARIES 	:= android_native_app_glue png

include $(BUILD_SHARED_LIBRARY)

$(call import-add-path, $(PROJECT_ROOT_PATH))
$(call import-module, android/native_app_glue)
$(call import-module, third_party/png)