/*
 * GLTexture.h
 *
 *  Created on: 03.12.2014
 *      Author: DerRM
 */

#ifndef GLTEXTURE_H_
#define GLTEXTURE_H_

#include <GLES2/gl2.h>

namespace chs {

class GLTexture {
public:
	GLTexture();

	GLuint loadTexture(const GLsizei width, const GLsizei height,
					   const GLenum type, const GLvoid* pixels);
};

} /* namespace chs */

#endif /* GLTEXTURE_H_ */
