/*
 * GLRenderer.cpp
 *
 *  Created on: 03.12.2014
 *      Author: DerRM
 */

#include "renderer/GLRenderer.h"
#include "resource/OBJLoader.h"

namespace chs {

GLRenderer::GLRenderer()
: texture(0),
  program(0),
  vbo(0),
  a_position_location(0),
  a_texture_coordinates_location(0),
  u_texture_unit_location(0),
  m_pApp(0)
{
}

void GLRenderer::onCreate()
{
	glClearColor(0.39f, 0.58f, 0.93f, 1.0f);

	PNGLoader pngLoader(m_pApp, "png/Android_robot.png");
	RawImageData image = pngLoader.getRawImageData();
	GLTexture glTexture;

	texture = glTexture.loadTexture(image.width, image.height, 0, image.data);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	Shader shader;
	program = shader.initShader(m_pApp, "shader/hello.vert", "shader/hello.frag");

	a_position_location = glGetAttribLocation(program, "a_Position");
	a_texture_coordinates_location = glGetAttribLocation(program, "a_TextureCoordinates");
	u_texture_unit_location = glGetUniformLocation(program, "u_TextureUnit");

//	OBJLoader objLoader;
//	objLoader.loadMesh(m_pApp, "mesh/cube.obj.png");
}

void GLRenderer::onDrawFrame()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(program);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1i(u_texture_unit_location, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glVertexAttribPointer(a_position_location, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)0);
	glVertexAttribPointer(a_texture_coordinates_location, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(a_position_location);
	glEnableVertexAttribArray(a_texture_coordinates_location);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GLRenderer::setAndroidApp(android_app* app)
{
	m_pApp = app;
}

} /* namespace chs */
