/*
 * GLExtensionHelper.h
 *
 *  Created on: 08.11.2014
 *      Author: DerRM
 */

#ifndef GLEXTENSIONHELPER_H_
#define GLEXTENSIONHELPER_H_

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

namespace chs
{

class GLExtensionHelper
{
public:
	GLExtensionHelper();
	~GLExtensionHelper();

	GLuint GetNumberExtensions() const { return m_numberExtensions; }
	GLubyte** GetGlExtensionArray() const { return m_pGlExtensionArray; }

private:
	GLubyte* m_GlExtensionString;
	GLint m_numberExtensions;
	GLubyte** m_pGlExtensionArray;

	void init();
};

} /* namespace chs */

#endif /* GLEXTENSIONHELPER_H_ */
