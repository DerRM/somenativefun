/*
 * Shader.h
 *
 *  Created on: 24.08.2014
 *      Author: DerRM
 */

#ifndef SHADER_H_
#define SHADER_H_

#include <GLES2/gl2.h>
#include <android_native_app_glue.h>
#include "resource/ResourceManager.h"

namespace chs {

class Shader
{
public:
	GLuint initShader(android_app* pApplication, const char* vertexShader, const char* fragmentShader);
private:
	char* readShaderString(AAsset* pAsset);
	ResourceManager m_ResourceManager;
};

} /* namespace chs */

#endif /* SHADER_H_ */
