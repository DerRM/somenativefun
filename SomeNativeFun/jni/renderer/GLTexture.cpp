/*
 * GLTexture.cpp
 *
 *  Created on: 03.12.2014
 *      Author: DerRM
 */

#include <renderer/GLTexture.h>

namespace chs {

GLTexture::GLTexture()
{
}

GLuint GLTexture::loadTexture(const GLsizei width, const GLsizei height,
							  const GLenum type, const GLvoid* pixels)
{
	GLuint textureObjectId;
	glGenTextures(1, &textureObjectId);

	glBindTexture(GL_TEXTURE_2D, textureObjectId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	return textureObjectId;
}

} /* namespace chs */
