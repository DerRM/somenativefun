/*
 * Shader.cpp
 *
 *  Created on: 24.08.2014
 *      Author: DerRM
 */

#include "Shader.h"
#include "resource/ResourceManager.h"
#include "Log.h"

namespace chs {

GLuint Shader::initShader(android_app* pApplication, const char* vertexShader, const char* fragmentShader)
{
	GLint status;

	char* vertexFile = readShaderString(m_ResourceManager.openFile(pApplication, vertexShader));
	GLuint vertexHandle = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexHandle, 1, (const GLchar **) &vertexFile, NULL);
	glCompileShader(vertexHandle);

	glGetShaderiv(vertexHandle, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE)
	{
		GLchar logBuf[1024];
		glGetShaderInfoLog(vertexHandle, sizeof(logBuf), NULL, logBuf);
		LOG_E("Shader Error: %s", logBuf);
	}

	free(vertexFile);
	m_ResourceManager.closeFile();

	char* fragFile = readShaderString(m_ResourceManager.openFile(pApplication, fragmentShader));
	GLuint fragmentHandle = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentHandle, 1, (const GLchar **) &fragFile, NULL);
	glCompileShader(fragmentHandle);

	glGetShaderiv(fragmentHandle, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE)
	{
		GLchar logBuf[1024];
		glGetShaderInfoLog(fragmentHandle, sizeof(logBuf), NULL, logBuf);
		LOG_E("Shader Error: %s", logBuf);
	}

	free(fragFile);
	m_ResourceManager.closeFile();

	GLuint programHandle = glCreateProgram();
	glAttachShader(programHandle, vertexHandle);
	glAttachShader(programHandle, fragmentHandle);
	glLinkProgram(programHandle);

	return programHandle;
}

char* Shader::readShaderString(AAsset* pAsset)
{
	size_t size = AAsset_getLength(pAsset);
    char* buffer = (char*)malloc (sizeof(char)*size + 1);
	AAsset_read(pAsset, buffer, size);
	buffer[size] = 0;
	return buffer;
}

} /* namespace chs */

