/*
 * GLExtensionHelper.cpp
 *
 *  Created on: 08.11.2014
 *      Author: DerRM
 */

#include <renderer/GLExtensionHelper.h>
#include <string.h>

//#ifdef NDK_DEBUG
#include "Log.h"
//#endif

namespace chs
{

GLExtensionHelper::GLExtensionHelper()
: m_numberExtensions(0),
  m_pGlExtensionArray(0)
{
	init();
}

GLExtensionHelper::~GLExtensionHelper()
{
	delete m_GlExtensionString;
}

void GLExtensionHelper::init()
{
	const GLubyte* glExtensionString = glGetString(GL_EXTENSIONS);

	m_GlExtensionString = (GLubyte*) malloc(sizeof(GLubyte) * strlen((const char*)glExtensionString) + 1);
	m_pGlExtensionArray = (GLubyte**) malloc(sizeof(char**) * 100);

	int count = 0;

	while(*glExtensionString)
	{
		*m_GlExtensionString = *glExtensionString;

		if(count == 0)
		{
			m_pGlExtensionArray[m_numberExtensions] = m_GlExtensionString;
		}

		if (*glExtensionString == ' ')
		{
			*m_GlExtensionString = '\0';
			m_numberExtensions++;

			count = 0;
			m_GlExtensionString++;
			glExtensionString++;

			continue;
		}

		count++;
		m_GlExtensionString++;
		glExtensionString++;
	}

	for (int strings = 0; strings < m_numberExtensions; ++strings)
	{
		LOG_I("%s", m_pGlExtensionArray[strings]);
	}
}

} /* namespace chs */
