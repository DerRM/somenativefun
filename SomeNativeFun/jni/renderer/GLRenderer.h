/*
 * GLRenderer.h
 *
 *  Created on: 03.12.2014
 *      Author: DerRM
 */

#ifndef GLRENDERER_H_
#define GLRENDERER_H_

#include <GLES2/gl2.h>
#include "GLTexture.h"
#include "Shader.h"
#include "resource/PNGLoader.h"

namespace chs {

class GLRenderer {
public:
	GLRenderer();

	void setAndroidApp(android_app* app);
	void onCreate();
	void onDrawFrame();

	GLuint texture;
	GLuint program;
	GLuint vbo;

	GLint a_position_location;
	GLint a_texture_coordinates_location;
	GLint u_texture_unit_location;
private:
	android_app* m_pApp;
};

static const float rect[] = {-1.0f, -1.0f, 0.0f, 0.0f,
                             -1.0f,  1.0f, 0.0f, 1.0f,
                              1.0f, -1.0f, 1.0f, 0.0f,
                              1.0f,  1.0f, 1.0f, 1.0f};
} /* namespace chs */

#endif /* GLRENDERER_H_ */
