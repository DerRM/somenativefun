@ Add Vector with neon
.text
.global add_neon

pResult .req r0
pVec1 	.req r1
pVec2 	.req r2

add_neon:

	@vld1.f32 	{q0}, [pVec1]
	@vld1.f32	{q1}, [pVec2]
	vadd.f32 	q0, q0, q1
	@vst1.f32	{q0}, [pResult]

	bx			lr
