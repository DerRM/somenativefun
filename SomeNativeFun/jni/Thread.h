/*
 * Thread.h
 *
 *  Created on: 15.06.2014
 *      Author: DerRM
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <pthread.h>
#include <unistd.h>

#include "Log.h"

namespace chs
{
	class CThread {
	public:
		CThread();
		virtual ~CThread();

		void startThreads();
	private:
		static void* runByThread(void* args);
	};
}
#endif /* THREAD_H_ */
