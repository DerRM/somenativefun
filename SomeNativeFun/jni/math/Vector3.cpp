/*
 * Vector3.cpp
 *
 *  Created on: 04.04.2014
 *      Author: DerRM
 */

#include <cmath>

#include "Vector3.h"

namespace chs
{

Vector3::Vector3() :
		x(0.0f), y(0.0f), z(0.0f), w(0.0f)
{
}

Vector3::Vector3(float x, float y, float z) :
		x(x), y(y), z(z), w(0.0f)
{
}

void Vector3::init()
{
	//crossPtr = &cross_neon;
}

} /* namespace chs */
