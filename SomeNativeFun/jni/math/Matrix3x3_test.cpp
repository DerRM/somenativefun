/*
 * Matrix3x3_test.cpp
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#include <gtest/gtest.h>
#include <math/Matrix3x3.h>

using namespace chs;

TEST(Matrix3x3, Determinant)
{
	Matrix3x3 matrix(6, 1, 1,
					 4, -2, 5,
					 2, 8, 7);
	float result = matrix.determinant();

	EXPECT_FLOAT_EQ(-306.0f, result);
}

TEST(Matrix3x3, MatrixTimesMatrix)
{
	Matrix3x3 matrix0(3, -1, 2,
					  0, 4, 1,
					  5, 3, -4);
	Matrix3x3 matrix1(6, 11, 4,
					  9, 0, 3,
					  1, 6, 2);

	Matrix3x3 expected(11, 45, 13,
					   37, 6, 14,
					   53, 31, 21);

	Matrix3x3 result = matrix0 * matrix1;

	EXPECT_FLOAT_EQ(expected[0], result[0]);
	EXPECT_FLOAT_EQ(expected[1], result[1]);
	EXPECT_FLOAT_EQ(expected[2], result[2]);

	EXPECT_FLOAT_EQ(expected[3], result[3]);
	EXPECT_FLOAT_EQ(expected[4], result[4]);
	EXPECT_FLOAT_EQ(expected[5], result[5]);

	EXPECT_FLOAT_EQ(expected[6], result[6]);
	EXPECT_FLOAT_EQ(expected[7], result[7]);
	EXPECT_FLOAT_EQ(expected[8], result[8]);
}

TEST(Matrix3x3, Inverse)
{
	Matrix3x3 matrix(1, 2, 0,
			         2, 4, 1,
			         2, 1, 0);

	Matrix3x3 expected(-1.0f / 3.0f, 0, 2.0f / 3.0f,
					   2.0f / 3.0f, 0.0f, -1.0f / 3.0f,
					   -2, 1, 0);

	Matrix3x3 result = matrix.inverse();

	EXPECT_FLOAT_EQ(expected[0], result[0]);
	EXPECT_FLOAT_EQ(expected[1], result[1]);
	EXPECT_FLOAT_EQ(expected[2], result[2]);

	EXPECT_FLOAT_EQ(expected[3], result[3]);
	EXPECT_FLOAT_EQ(expected[4], result[4]);
	EXPECT_FLOAT_EQ(expected[5], result[5]);

	EXPECT_FLOAT_EQ(expected[6], result[6]);
	EXPECT_FLOAT_EQ(expected[7], result[7]);
	EXPECT_FLOAT_EQ(expected[8], result[8]);

//	std::cout << expected << "\n";
//	std::cout << result << "\n";
}

TEST(Matrix3x3, Index)
{
	Matrix3x3 matrix(1, 2, 3,
					 4, 5, 6,
					 7, 8, 9);

	float value = matrix.getValue(2, 1);

	EXPECT_FLOAT_EQ(8, value);

	value = matrix.getValue(1, 0);

	EXPECT_FLOAT_EQ(4, value);
}
