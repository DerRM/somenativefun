/*
 * Vector4.h
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#ifndef VECTOR4_H_
#define VECTOR4_H_

#include <ostream>

namespace chs
{

struct Vector4
{
	float x, y, z, w;

	Vector4();
	Vector4(float x, float y, float z, float w);

	Vector4 normalize() const;
	float magnitude() const;
	float magnitude_sqr() const;
	const Vector4 operator /(float value) const;
	Vector4& operator /=(float value);
	const Vector4 operator -() const;
	const Vector4 operator *(float scale) const;
	Vector4& operator *=(float scale);
	const float dot(const Vector4& v) const;
	const Vector4 modulate(const Vector4& v) const;
	Vector4& operator +=(const Vector4& vec);
	Vector4& operator -=(const Vector4& vec);

	static Vector4 zero();
	static Vector4 ones();
};

std::ostream& operator <<(std::ostream& strm, const Vector4& v);
const Vector4 operator +(const Vector4& v0, const Vector4& v1);
const Vector4 operator -(const Vector4& v0, const Vector4& v1);

extern "C"
{
	Vector4 add_neon(Vector4 a, Vector4 b);
}

} /* namespace chs */

#endif /* VECTOR4_H_ */
