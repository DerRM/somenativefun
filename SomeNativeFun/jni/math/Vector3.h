/*
 * Vector3.h
 *
 *  Created on: 04.04.2014
 *      Author: DerRM
 */

#ifndef VECTOR3_H_
#define VECTOR3_H_

#define NEON_API extern "C"

#include <ostream>

namespace chs
{

struct Vector3;
typedef Vector3 (*crossFunc)(Vector3 v1, Vector3 v2);

struct Vector3
{
	float x, y, z, w;

	Vector3();
	Vector3(float x, float y, float z);


	Vector3 normalize() const;
	float magnitude() const;
	float magnitude_sqr() const;
	const Vector3 operator /(float value) const;
	Vector3& operator /=(float value);
	const Vector3 operator -() const;
	const Vector3 operator *(float scale) const;
	Vector3& operator *=(float scale);
	const float dot(const Vector3& v) const;
	const Vector3 cross(Vector3 v) const;
	const Vector3 modulate(const Vector3& v) const;
	Vector3& operator +=(const Vector3& vec);
	Vector3& operator -=(const Vector3& vec);

	static Vector3 zero();
	static Vector3 ones();

private:
	void init();
};

const Vector3 operator +(const Vector3& v0, const Vector3& v1);
const Vector3 operator -(const Vector3& v0, const Vector3& v1);
std::ostream& operator <<(std::ostream& strm, const Vector3& vec);

void cross_normal(Vector3& result, const Vector3& v1, const Vector3& v2);

NEON_API Vector3 cross_neon(Vector3 v1, Vector3 v2);
NEON_API Vector3 add_neon(Vector3 v1, Vector3 v2);

inline float Vector3::magnitude() const
{
	return std::sqrt(magnitude_sqr());
}

inline float Vector3::magnitude_sqr() const
{
	return x * x + y * y + z * z;
}

inline Vector3 Vector3::normalize() const
{
	return *this / magnitude();
}

inline const Vector3 Vector3::operator /(float value) const
{
	return Vector3(x / value, y / value, z / value);
}

inline const Vector3 Vector3::operator -() const
{
	return Vector3(-x, -y, -z);
}

inline const Vector3 Vector3::operator *(float scale) const
{
	return Vector3(x * scale, y * scale, z * scale);
}

inline const float Vector3::dot(const Vector3& v) const
{
	return x * v.x + y * v.y + z * v.z;
}

inline const Vector3 Vector3::cross(Vector3 v) const
{
	Vector3 result;
	cross_normal(result, *this, v);
	return result;
}

inline const Vector3 Vector3::modulate(const Vector3& v) const
{
	return Vector3(x * v.x, y * v.y, z * v.z);
}

inline Vector3& Vector3::operator /=(float value)
{
	x /= value;
	y /= value;
	z /= value;

	return *this;
}

inline Vector3& Vector3::operator *=(float scale)
{
	x *= scale;
	y *= scale;
	z *= scale;

	return *this;
}

inline Vector3& Vector3::operator +=(const Vector3& vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;

	return *this;
}

inline Vector3& Vector3::operator -=(const Vector3& vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;

	return *this;
}

inline Vector3 Vector3::zero()
{
	return Vector3();
}

inline Vector3 Vector3::ones()
{
	return Vector3(1.0f,1.0f,1.0f);
}

inline const Vector3 operator +(const Vector3& v0, const Vector3& v1)
{
	return Vector3(v0.x + v1.x, v0.y + v1.y, v0.z + v1.z);
}

inline const Vector3 operator -(const Vector3& v0, const Vector3& v1)
{
	return Vector3(v0.x - v1.x, v0.y - v1.y, v0.z - v1.z);
}

inline std::ostream& operator <<(std::ostream& strm, const Vector3& vec)
{
	strm << vec.x << " " << vec.y << " " << vec.z;
	return strm;
}

inline void cross_normal(Vector3& result, const Vector3& v1, const Vector3& v2)
{
	result.x = v1.y * v2.z - v1.z * v2.y;
	result.y = v1.z * v2.x - v1.x * v2.z;
	result.z = v1.x * v2.y - v1.y * v2.x;
}

} /* namespace chs */
#endif /* VECTOR3_H_ */
