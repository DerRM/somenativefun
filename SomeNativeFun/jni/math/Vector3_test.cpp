/*
 * Vector3_test.cpp
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#include <gtest/gtest.h>
#include <math/Vector3.h>

using namespace chs;

TEST(Vector3, CrossProduct)
{
	Vector3 v1 = Vector3(3, -3, 1);
	Vector3 v2 = Vector3(4, 9, 2);
	Vector3 expected = Vector3(-15, -2, 39);
	Vector3 result = cross_neon(v1, v2);

	EXPECT_EQ(expected.x, result.x);
	EXPECT_EQ(expected.y, result.y);
	EXPECT_EQ(expected.z, result.z);

	v1 = Vector3(2, 3, 4);
	v2 = Vector3(5, 6, 7);
	expected = Vector3(-3, 6, -3);
	result = cross_neon(v1, v2);

	EXPECT_EQ(expected.x, result.x);
	EXPECT_EQ(expected.y, result.y);
	EXPECT_EQ(expected.z, result.z);
}

TEST(Vector3, CrossPerfNeon)
{
	Vector3 v1 = Vector3(3, -3, 1);
	Vector3 v2 = Vector3(4, 9, 2);
	Vector3 result;

	for (int i = 0; i < 1000000; ++i)
	{
		 result = cross_neon(v1, v2);
	}
}

TEST(Vector3, CrossPerf)
{
	Vector3 v1 = Vector3(3, -3, 1);
	Vector3 v2 = Vector3(4, 9, 2);
	Vector3 result;

	for (int i = 0; i < 1000000; ++i)
	{
		result = v1.cross(v2);
	}
}
