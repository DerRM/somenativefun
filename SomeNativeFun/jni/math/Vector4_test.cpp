/*
 * Vector4_test.cpp
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#include <gtest/gtest.h>
#include <math/Vector4.h>

using namespace chs;

TEST(Vector4, Add)
{
	Vector4 v1 = Vector4(5,42,233,34);
	Vector4 v2 = Vector4(434,43,22,241);
	Vector4 result = add_neon(v1, v2);

	std::cout << "V1: " << v1 << " V2: " << v2 << " Result: " << result << "\n";

	EXPECT_EQ(v1.x + v2.x, result.x);
	EXPECT_EQ(v1.y + v2.y, result.y);
	EXPECT_EQ(v1.z + v2.z, result.z);
	EXPECT_EQ(v1.w + v2.w, result.w);
}

TEST(Vector4, AddPerfNeon)
{
	Vector4 v1 = Vector4(5,42,233,34);
	Vector4 v2 = Vector4(434,43,22,241);

	for (int i = 0; i < 1000000; ++i)
	{
		Vector4 result = add_neon(v1, v2);
	}
}

TEST(Vector4, AddPerf)
{
	Vector4 v1 = Vector4(5,42,233,34);
	Vector4 v2 = Vector4(434,43,22,241);

	for (int i = 0; i < 1000000; ++i)
	{
		Vector4 result = v1 + v2;
	}
}
