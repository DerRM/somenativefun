/*
 * Quaternion.cpp
 *
 *  Created on: 22.06.2014
 *      Author: DerRM
 */

#include <math/Quaternion.h>

namespace chs
{

Quaternion::Quaternion() :
		w(0.0f), x(0.0f), y(0.0f), z(0.0f)
{
}

Quaternion::Quaternion(float w, float x, float y, float z) :
		w(w), x(x), y(y), z(z)
{
	normalize();
}

void Quaternion::normalize()
{
	double length = sqrt(
			  this->w * this->w
			+ this->x * this->x
			+ this->y * this->y
			+ this->z * this->z);

	if (fabs(length) > TOLERANCE && fabs(length - 1.0) > TOLERANCE)
	{
		double invLength = 1.0 / length;
		this->w *= invLength;
		this->x *= invLength;
		this->y *= invLength;
		this->z *= invLength;
	}
}

Quaternion Quaternion::conjugate() const
{
	Quaternion quat;
	quat.w = this->w;
	quat.x = -this->x;
	quat.y = -this->y;
	quat.z = -this->z;
	return quat;
}

Quaternion Quaternion::inverse() const
{
	return Quaternion();
}

Matrix3x3 Quaternion::toMatrix3x3() const
{
	Matrix3x3 matrix;

	matrix[0] = 1.0f - 2.0f * this->y * this->y - 2.0f * this->z * this->z;
	matrix[3] = 2.0f * this->x * this->y - 2.0f * this->z * this->w;
	matrix[6] = 2.0f * this->x * this->z + 2.0f * this->y * this->w;

	matrix[1] = 2.0f * this->x * this->y + 2.0f * this->z * this->w;
	matrix[4] = 1.0f - 2.0f * this->x * this->x - 2.0f * this->z * this->z;
	matrix[7] = 2.0f * this->y * this->z - 2.0f * this->x * this->w;

	matrix[2] = 2.0f * this->x * this->z - 2.0f * this->y * this->w;
	matrix[5] = 2.0f * this->y * this->z + 2.0f * this->x * this->w;
	matrix[8] = 1.0f - 2.0f * this->x * this->x - 2.0f * this->y * this->y;

	return matrix;
}

Matrix4x4 Quaternion::toMatrix4x4() const
{
	Matrix4x4 matrix;

	matrix[0] = 1.0f - 2.0f * this->y * this->y - 2.0f * this->z * this->z;
	matrix[4] = 2.0f * this->x * this->y - 2.0f * this->z * this->w;
	matrix[8] = 2.0f * this->x * this->z + 2.0f * this->y * this->w;

	matrix[1] = 2.0f * this->x * this->y + 2.0f * this->z * this->w;
	matrix[5] = 1.0f - 2.0f * this->x * this->x - 2.0f * this->z * this->z;
	matrix[9] = 2.0f * this->y * this->z - 2.0f * this->x * this->w;

	matrix[2] = 2.0f * this->x * this->z - 2.0f * this->y * this->w;
	matrix[6] = 2.0f * this->y * this->z + 2.0f * this->x * this->w;
	matrix[10] = 1.0f - 2.0f * this->x * this->x - 2.0f * this->y * this->y;

	return matrix;
}

const Quaternion Quaternion::operator -() const
{
	return Quaternion(-w, -x, -y, -z);
}

const Quaternion Quaternion::operator *(float scale) const
{
	return Quaternion(w * scale, x * scale, y * scale, z * scale);
}

const Quaternion Quaternion::operator /(float scale) const
{
	return Quaternion(w / scale, x / scale, y / scale, z / scale);
}

Quaternion fromEuler(const Vector3& angleAxis)
{
	return fromEuler(angleAxis.x, angleAxis.y, angleAxis.z);
}

Quaternion fromEuler(float x, float y, float z)
{
	x *= DEG2RAD;
	y *= DEG2RAD;
	z *= DEG2RAD;

	float xOver2 = x * 0.5f;
	float yOver2 = y * 0.5f;
	float zOver2 = z * 0.5f;

	float sinXOver2 = (float) sin(xOver2);
	float cosXOver2 = (float) cos(xOver2);

	float sinYOver2 = (float) sin(yOver2);
	float cosYOver2 = (float) cos(yOver2);

	float sinZOver2 = (float) sin(zOver2);
	float cosZOver2 = (float) cos(zOver2);

	Quaternion quat;
	quat.w = cosYOver2 * cosXOver2 * cosZOver2
			+ sinYOver2 * sinXOver2 * sinZOver2;
	quat.x = cosYOver2 * sinXOver2 * cosZOver2
			- sinYOver2 * cosXOver2 * sinZOver2;
	quat.y = sinYOver2 * cosXOver2 * cosZOver2
			+ cosYOver2 * sinXOver2 * sinZOver2;
	quat.z = cosYOver2 * cosXOver2 * sinZOver2
			- sinYOver2 * sinXOver2 * cosZOver2;

	quat.normalize();

	return quat;
}

Quaternion operator *(const Quaternion& q0, const Quaternion& q1)
{
	return Quaternion(q0.w * q1.w - q0.x * q1.x - q0.y * q1.y - q0.z * q1.z,
					  q0.w * q1.x + q0.x * q1.w + q0.y * q1.z - q0.z * q1.y,
					  q0.w * q1.y + q0.y * q1.w + q0.z * q1.x - q0.x * q1.z,
					  q0.w * q1.z + q0.z * q1.w + q0.x * q1.y - q0.y * q1.x);
}

Vector3 operator *(const Quaternion& q, const Vector3& v)
{
	Vector3 vec = v.normalize();
	Quaternion vecQuat, resQuat;

	vecQuat.x = vec.x;
	vecQuat.y = vec.y;
	vecQuat.z = vec.z;
	vecQuat.w = 0.0f;

	resQuat = vecQuat * q.conjugate();
	resQuat = q * resQuat;

	return Vector3(resQuat.x, resQuat.y, resQuat.z);
}

bool operator ==(const Quaternion& q0, const Quaternion& q1)
{
	return (q0.x == q1.x && q0.y == q1.y && q0.z == q1.z && q0.w == q1.w);
}

Quaternion operator +(const Quaternion& q0, const Quaternion& q1)
{
	return Quaternion(q0.w + q1.w, q0.x + q1.x, q0.y + q1.y, q0.z + q1.z);
}

Quaternion lerp(const Quaternion& q0, const Quaternion& q1, float t)
{
	Quaternion quat = (q0 * (1.0f - t) + q1 * t);
	quat.normalize();
	return quat;
}

Quaternion slerp(const Quaternion& q0, const Quaternion& q1, float t)
{
	float dot = q0.x * q1.x + q0.y * q1.y + q0.z * q1.z + q0.w * q1.w;

	Quaternion q2;

	if (dot < 0)
	{
		dot = -dot;
		q2 = -q1;
	}
	else
	{
		q2 = q1;
	}

	if (dot < 0.95f)
	{
		float angle = acos(dot);
		return (q0 * sinf(angle * (1.0f - t)) + q2 * sinf(angle * t)) / sinf(angle);
	}
	else
	{
		return lerp(q0, q2, t);
	}

	return Quaternion();
}

} /* namespace chs */
