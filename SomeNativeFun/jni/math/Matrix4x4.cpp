/*
 * Matrix4x4.cpp
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#include "math/Matrix4x4.h"
#include "math/Vector4.h"

namespace chs
{

Matrix4x4::Matrix4x4()
{
	set(1.0f, 0.0f, 0.0f, 0.0f,
	    0.0f, 1.0f, 0.0f, 0.0f,
	    0.0f, 0.0f, 1.0f, 0.0f,
	    0.0f, 0.0f, 0.0f, 1.0f);
}

Matrix4x4::Matrix4x4(float m00, float m01, float m02, float m03,
		  	  	  	 float m10, float m11, float m12, float m13,
		  	  	  	 float m20, float m21, float m22, float m23,
		  	  	  	 float m30, float m31, float m32, float m33)
{
	set(m00, m01, m02, m03,
		m10, m11, m12, m13,
		m20, m21, m22, m23,
		m30, m31, m32, m33);
}

Matrix4x4::Matrix4x4(float (&array)[16])
{
	set(array[0], array[1], array[2], array[3],
	    array[4], array[5], array[6], array[7],
	    array[8], array[9], array[10], array[11],
	    array[12], array[13], array[14], array[15]);
}

Matrix4x4::~Matrix4x4()
{
}

void Matrix4x4::set(float m00, float m01, float m02, float m03,
		 	 	 	float m10, float m11, float m12, float m13,
		 	 	 	float m20, float m21, float m22, float m23,
		 	 	 	float m30, float m31, float m32, float m33)
{
	this->m[0] = m00; this->m[4] = m01; this->m[8] = m02; this->m[12] = m03;
	this->m[1] = m10; this->m[5] = m11; this->m[9] = m12; this->m[13] = m13;
	this->m[2] = m20; this->m[6] = m21; this->m[10] = m22; this->m[14] = m23;
	this->m[3] = m30; this->m[7] = m31; this->m[11] = m32; this->m[15] = m33;
}

Vector4 Matrix4x4::getRow(int row)
{
	if (row < 4)
	{
		return Vector4(m[row + 0], m[row + 4], m[row + 8], m[row + 12]);
	}

	return Vector4();
}

void Matrix4x4::setRow(int row, const Vector4& vec)
{
	if (row < 4)
	{
		m[row + 0] = vec.x;
		m[row + 4] = vec.y;
		m[row + 8] = vec.z;
		m[row + 12] = vec.w;
	}
}

Vector4 Matrix4x4::getColumn(int col)
{
	if (col < 4)
	{
		return Vector4(m[col + 0], m[col + 1], m[col + 2], m[col + 3]);
	}

	return Vector4();
}

void Matrix4x4::setColumn(int col, const Vector4& vec)
{
	if (col < 4)
	{
		m[col + 0] = vec.x;
		m[col + 1] = vec.y;
		m[col + 2] = vec.z;
		m[col + 3] = vec.w;
	}
}

Matrix4x4 Matrix4x4::inverse()
{
	return Matrix4x4();
}

Matrix4x4 Matrix4x4::transpose()
{
	Matrix4x4 transpose;

//	transpose[0] = m[0];
	transpose[1] = m[4];
	transpose[2] = m[8];
	transpose[3] = m[12];

	transpose[4] = m[1];
//	transpose[5] = m[5];
	transpose[6] = m[9];
	transpose[7] = m[13];

	transpose[8] = m[2];
	transpose[9] = m[6];
//	transpose[10] = m[10];
	transpose[11] = m[14];

	transpose[12] = m[3];
	transpose[13] = m[7];
	transpose[14] = m[11];
//	transpose[15] = m[15];

	return transpose;
}

float Matrix4x4::determinant()
{
	return m[0] * getCofactor(m[5], m[6], m[7], m[9], m[10], m[11], m[13], m[14], m[15]) -
		   m[1] * getCofactor(m[4], m[6], m[7], m[8], m[10], m[11], m[12], m[14], m[15]) +
		   m[2] * getCofactor(m[4], m[5], m[7], m[8], m[9],  m[11], m[12], m[13], m[15]) -
		   m[3] * getCofactor(m[4], m[5], m[6], m[8], m[9],  m[10], m[12], m[13], m[14]);
}

float Matrix4x4::getCofactor(float m0, float m1, float m2,
		  	  	  	  	  	 float m3, float m4, float m5,
		  	  	  	  	  	 float m6, float m7, float m8)
{
	return m0 * (m4 * m8 - m7 * m5) -
		   m1 * (m3 * m8 - m6 * m5) +
		   m2 * (m3 * m7 - m6 * m4);
}

float Matrix4x4::operator [](int index) const
{
	return m[index];
}

float& Matrix4x4::operator [](int index)
{
	return m[index];
}

Matrix4x4 Matrix4x4::identity()
{
	return Matrix4x4();
}

Matrix4x4 Matrix4x4::zero()
{
	return Matrix4x4(0, 0, 0, 0,
			   	     0, 0, 0, 0,
			   	     0, 0, 0, 0,
			   	     0, 0, 0, 0);
}

Matrix4x4 Matrix4x4::ones()
{
	return Matrix4x4(1, 1, 1, 1,
			   	     1, 1, 1, 1,
			   	     1, 1, 1, 1,
			   	     1, 1, 1, 1);
}

Matrix4x4 operator *(const Matrix4x4& m0, const Matrix4x4& m1)
{
	Matrix4x4 m;

	m[0] = m0[0] * m1[0] + m0[4] * m1[1] + m0[8] * m1[2] + m0[12] * m1[3];
	m[4] = m0[0] * m1[4] + m0[4] * m1[5] + m0[8] * m1[6] + m0[12] * m1[7];
	m[8] = m0[0] * m1[8] + m0[4] * m1[9] + m0[8] * m1[10] + m0[12] * m1[11];
	m[12] = m0[0] * m1[12] + m0[4] * m1[13] + m0[8] * m1[14] + m0[12] * m1[15];

	m[1] = m0[1] * m1[0] + m0[5] * m1[1] + m0[9] * m1[2] + m0[13] * m1[3];
	m[5] = m0[1] * m1[4] + m0[5] * m1[5] + m0[9] * m1[6] + m0[13] * m1[7];
	m[9] = m0[1] * m1[8] + m0[5] * m1[9] + m0[9] * m1[10] + m0[13] * m1[11];
	m[13] = m0[1] * m1[12] + m0[5] * m1[13] + m0[9] * m1[14] + m0[13] * m1[15];

	m[2] = m0[2] * m1[0] + m0[6] * m1[1] + m0[10] * m1[2] + m0[14] * m1[3];
	m[6] = m0[2] * m1[4] + m0[6] * m1[5] + m0[10] * m1[6] + m0[14] * m1[7];
	m[10] = m0[2] * m1[8] + m0[6] * m1[9] + m0[10] * m1[10] + m0[14] * m1[11];
	m[14] = m0[2] * m1[12] + m0[6] * m1[13] + m0[10] * m1[14] + m0[14] * m1[15];

	m[3] = m0[3] * m1[0] + m0[7] * m1[1] + m0[11] * m1[2] + m0[15] * m1[3];
	m[7] = m0[3] * m1[4] + m0[7] * m1[5] + m0[11] * m1[6] + m0[15] * m1[7];
	m[11] = m0[3] * m1[8] + m0[7] * m1[9] + m0[11] * m1[10] + m0[15] * m1[11];
	m[15] = m0[3] * m1[12] + m0[7] * m1[13] + m0[11] * m1[14] + m0[15] * m1[15];

	return m;
}

Vector4 operator *(const Matrix4x4& matrix, const Vector4& vec)
{
	Vector4 result;

	result.x = matrix[0] * vec.x + matrix[4] * vec.y + matrix[8] * vec.z + matrix[12] * vec.w;
	result.y = matrix[1] * vec.x + matrix[5] * vec.y + matrix[9] * vec.z + matrix[13] * vec.w;
	result.z = matrix[2] * vec.x + matrix[6] * vec.y + matrix[10] * vec.z + matrix[14] * vec.w;
	result.w = matrix[3] * vec.x + matrix[7] * vec.y + matrix[11] * vec.z + matrix[15] * vec.w;

	return result;
}

std::ostream& operator <<(std::ostream& strm, const Matrix4x4& m)
{
	strm << m[0] << " " << m[4] << " " << m[8] << " " << m[12] << "\n";
	strm << m[1] << " " << m[5] << " " << m[9] << " " << m[13] << "\n";
	strm << m[2] << " " << m[6] << " " << m[10] << " " << m[14] << "\n";
	strm << m[3] << " " << m[7] << " " << m[11] << " " << m[15];

	return strm;
}

} /* namespace chs */
