/*
 * Matrix3x3.cpp
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#include "math/Matrix3x3.h"
#include "math/Vector3.h"

const float EPSILON = 0.00001f;

namespace chs
{

Matrix3x3::Matrix3x3()
{
	set(1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f);
}

Matrix3x3::Matrix3x3(float m00, float m01, float m02,
			  	  	 float m10, float m11, float m12,
			  	  	 float m20, float m21, float m22)

{
	set(m00, m01, m02,
		m10, m11, m12,
		m20, m21, m22);
}

Matrix3x3::Matrix3x3(float (&array)[9])
{
	set(array[0], array[3], array[6],
	    array[1], array[4], array[7],
	    array[2], array[5], array[8]);
}

Matrix3x3::~Matrix3x3()
{

}

void Matrix3x3::set(float m00, float m01, float m02,
				    float m10, float m11, float m12,
				    float m20, float m21, float m22)
{
	this->m[0] = m00; this->m[3] = m01; this->m[6] = m02;
	this->m[1] = m10; this->m[4] = m11; this->m[7] = m12;
	this->m[2] = m20; this->m[5] = m21; this->m[8] = m22;
}

Vector3 Matrix3x3::getRow(int row)
{
	if (row < 3)
	{
		return Vector3(m[row + 0], m[row + 3], m[row + 6]);
	}

	return Vector3();
}

void Matrix3x3::setRow(int row, const Vector3& vec)
{
	if (row < 3)
	{
		m[row + 0] = vec.x;
		m[row + 3] = vec.y;
		m[row + 6] = vec.z;
	}
}

Vector3 Matrix3x3::getColumn(int col)
{
	if (col < 3)
	{
		return Vector3(m[col + 0], m[col + 1], m[col + 2]);
	}

	return Vector3();
}

void Matrix3x3::setColumn(int col, const Vector3& vec)
{
	if (col < 3)
	{
		m[col + 0] = vec.x;
		m[col + 1] = vec.y;
		m[col + 2] = vec.z;
	}
}

float Matrix3x3::getValue(int row, int col)
{
	if (col < 3 && row < 3)
	{
		return m[row + 3 * col];
	}

	return std::numeric_limits<float>::quiet_NaN();
}

void Matrix3x3::setValue(int row, int col, float value)
{
	if (col < 3 && row < 3)
	{
		m[row + 3 * col] = value;
	}
}

Matrix3x3 Matrix3x3::inverse()
{
	float det, invDet;
	Matrix3x3 tmp;

	tmp[0] = m[4] * m[8] - m[5] * m[7];
	tmp[3] = m[5] * m[6] - m[3] * m[8];
	tmp[6] = m[3] * m[7] - m[4] * m[6];

	det = m[0] * tmp[0] + m[1] * tmp[3] + m[2] * tmp[6];

	if (fabs(det) <= EPSILON)
	{
		return identity();
	}

	tmp[1] = m[2] * m[7] - m[1] * m[8];
	tmp[2] = m[1] * m[5] - m[2] * m[4];
	tmp[4] = m[0] * m[8] - m[2] * m[6];
	tmp[5] = m[2] * m[3] - m[0] * m[5];
	tmp[7] = m[1] * m[6] - m[0] * m[7];
	tmp[8] = m[0] * m[4] - m[1] * m[3];

	invDet = 1.0f / det;

	Matrix3x3 inverse;

	inverse[0] = invDet * tmp[0];
	inverse[1] = invDet * tmp[1];
	inverse[2] = invDet * tmp[2];
	inverse[3] = invDet * tmp[3];
	inverse[4] = invDet * tmp[4];
	inverse[5] = invDet * tmp[5];
	inverse[6] = invDet * tmp[6];
	inverse[7] = invDet * tmp[7];
	inverse[8] = invDet * tmp[8];

	return inverse;
}

Matrix3x3 Matrix3x3::transpose()
{
	Matrix3x3 transpose;

//	transpose[0] = m[0];
	transpose[1] = m[3];
	transpose[2] = m[6];

	transpose[3] = m[1];
//	transpose[4] = m[4];
	transpose[5] = m[7];

	transpose[6] = m[2];
	transpose[7] = m[5];
//	transpose[8] = m[8];

	return transpose;
}

float Matrix3x3::determinant()
{
	return m[0] * (m[4] * m[8] - m[7] * m[5]) -
		   m[1] * (m[3] * m[8] - m[6] * m[5]) +
		   m[2] * (m[3] * m[7] - m[6] * m[4]);
}

float Matrix3x3::operator [](int index) const
{
	return m[index];
}

float& Matrix3x3::operator [](int index)
{
	return m[index];
}

Matrix3x3 Matrix3x3::identity()
{
	return Matrix3x3();
}

Matrix3x3 Matrix3x3::zero()
{
	return Matrix3x3(0, 0, 0,
			   	     0, 0, 0,
			   	     0, 0, 0);
}

Matrix3x3 Matrix3x3::ones()
{
	return Matrix3x3(1, 1, 1,
			   	     1, 1, 1,
			   	     1, 1, 1);
}

Matrix3x3 operator *(const Matrix3x3& m0, const Matrix3x3& m1)
{
	Matrix3x3 m;

	m[0] = m0[0] * m1[0] + m0[3] * m1[1] + m0[6] * m1[2];
	m[3] = m0[0] * m1[3] + m0[3] * m1[4] + m0[6] * m1[5];
	m[6] = m0[0] * m1[6] + m0[3] * m1[7] + m0[6] * m1[8];

	m[1] = m0[1] * m1[0] + m0[4] * m1[1] + m0[7] * m1[2];
	m[4] = m0[1] * m1[3] + m0[4] * m1[4] + m0[7] * m1[5];
	m[7] = m0[1] * m1[6] + m0[4] * m1[7] + m0[7] * m1[8];

	m[2] = m0[2] * m1[0] + m0[5] * m1[1] + m0[8] * m1[2];
	m[5] = m0[2] * m1[3] + m0[5] * m1[4] + m0[8] * m1[5];
	m[8] = m0[2] * m1[6] + m0[5] * m1[7] + m0[8] * m1[8];

	return m;
}

Vector3 operator *(const Matrix3x3& matrix, const Vector3& vec)
{
	Vector3 result;

	result.x = matrix[0] * vec.x + matrix[3] * vec.y + matrix[6] * vec.z;
	result.y = matrix[1] * vec.x + matrix[4] * vec.y + matrix[7] * vec.z;
	result.z = matrix[2] * vec.x + matrix[5] * vec.y + matrix[8] * vec.z;

	return result;
}

std::ostream& operator <<(std::ostream& strm, const Matrix3x3& m)
{
	strm << m[0] << " " << m[3] << " " << m[6] << "\n";
	strm << m[1] << " " << m[4] << " " << m[7] << "\n";
	strm << m[2] << " " << m[5] << " " << m[8];

	return strm;
}

} /* namespace chs */
