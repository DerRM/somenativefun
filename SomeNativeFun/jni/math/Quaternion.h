/*
 * Quaternion.h
 *
 *  Created on: 22.06.2014
 *      Author: DerRM
 */

#ifndef QUATERNION_H_
#define QUATERNION_H_

#include <cmath>

#include "Vector3.h"
#include "Matrix3x3.h"
#include "Matrix4x4.h"

#define RAD2DEG 180.0f / M_PI
#define DEG2RAD M_PI / 180.0f
#define TOLERANCE 0.00001f

namespace chs
{

struct Quaternion
{
	Quaternion();
	Quaternion(float w, float x, float y, float z);

	void normalize();
	Quaternion conjugate() const;
	Quaternion inverse() const;

	Matrix3x3 toMatrix3x3() const;
	Matrix4x4 toMatrix4x4() const;

	const Quaternion operator -() const;
	const Quaternion operator *(float scale) const;
	const Quaternion operator /(float scale) const;

	float w, x, y, z;
};

Quaternion fromEuler(const Vector3& angleAxis);
Quaternion fromEuler(float x, float y, float z);
Quaternion operator *(const Quaternion& q0, const Quaternion& q1);
Vector3 operator *(const Quaternion& q, const Vector3& v);
Quaternion lerp(const Quaternion& q0, const Quaternion& q1, float t);
Quaternion slerp(const Quaternion& q0, const Quaternion& q1, float t);
bool operator ==(const Quaternion& q0, const Quaternion& q1);
Quaternion operator +(const Quaternion& q0, const Quaternion& q1);

} /* namespace chs */

#endif /* QUATERNION_H_ */
