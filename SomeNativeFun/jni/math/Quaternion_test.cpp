/*
 * Quaternion_unittest.cpp
 *
 *  Created on: 13.07.2014
 *      Author: DerRM
 */

#include <gtest/gtest.h>

#include "Quaternion.h"
#include "Vector3.h"
#include "Matrix3x3.h"

using namespace chs;

TEST(Quaternion, Multiply)
{
	Quaternion quat(0.5f, 1.25f, 1.5f, 0.25f);
	Quaternion result = Quaternion(1.0f, 0.0f, 1.0f, 0.0f) * Quaternion(1.0f, 0.5f, 0.5f, 0.75f);

	EXPECT_FLOAT_EQ(quat.x, result.x);
	EXPECT_FLOAT_EQ(quat.y, result.y);
	EXPECT_FLOAT_EQ(quat.z, result.z);
	EXPECT_FLOAT_EQ(quat.w, result.w);
}

TEST(Quaternion, FromEuler)
{
	Quaternion quat(0.0f, -0.7071f, 0.7071f, 0.0f);
	Quaternion result = fromEuler(0.0f, 180.0f, 90.0f);

	EXPECT_NEAR(quat.x, result.x, 0.00001f);
	EXPECT_NEAR(quat.y, result.y, 0.00001f);
	EXPECT_NEAR(quat.z, result.z, 0.00001f);
	EXPECT_NEAR(quat.w, result.w, 0.00001f);
}

TEST(Quaternion, Rotation)
{
	Vector3 vec(1.0f, 0.0f, 0.0f);

	Quaternion quat = fromEuler(0.0f, 90.0f, 0.0f);
	Vector3 result = quat * Vector3(0.0f, 0.0f, 1.0f);

	EXPECT_FLOAT_EQ(result.x, vec.x);
	EXPECT_FLOAT_EQ(result.y, vec.y);
	EXPECT_FLOAT_EQ(result.z, vec.z);

	vec = Vector3(0.0f, 0.0f, 1.0f);

	quat = fromEuler(90.0f, 0.0f, 0.0f);
	result = quat * Vector3(0.0f, 1.0f, 0.0f);

	EXPECT_FLOAT_EQ(result.x, vec.x);
	EXPECT_FLOAT_EQ(result.y, vec.y);
	EXPECT_FLOAT_EQ(result.z, vec.z);

	vec = Vector3(-1.0f, 0.0f, 0.0f);

	quat = fromEuler(0.0f, 0.0f, 90.0f);
	result = quat * Vector3(0.0f, 1.0f, 0.0f);

	EXPECT_FLOAT_EQ(result.x, vec.x);
	EXPECT_FLOAT_EQ(result.y, vec.y);
	EXPECT_FLOAT_EQ(result.z, vec.z);
}

TEST(Quaternion, ToMatrix)
{
	Quaternion quat = fromEuler(0.0f, 0.0f, 90.0f);

	Matrix3x3 matrix(0, -1, 0,
					 1, 0, 0,
					 0, 0, 1);

	Matrix3x3 result = quat.toMatrix3x3();

	EXPECT_NEAR(matrix[0], result[0], 0.0000001f);
	EXPECT_NEAR(matrix[1], result[1], 0.0000001f);
	EXPECT_NEAR(matrix[2], result[2], 0.0000001f);

	EXPECT_NEAR(matrix[3], result[3], 0.0000001f);
	EXPECT_NEAR(matrix[4], result[4], 0.0000001f);
	EXPECT_NEAR(matrix[5], result[5], 0.0000001f);

	EXPECT_NEAR(matrix[6], result[6], 0.0000001f);
	EXPECT_NEAR(matrix[7], result[7], 0.0000001f);
	EXPECT_NEAR(matrix[8], result[8], 0.0000001f);

	quat = fromEuler(90.0f, 0.0f, 0.0f);

	matrix.set(1, 0, 0,
			   0, 0, -1,
			   0, 1, 0);

	result = quat.toMatrix3x3();

	EXPECT_NEAR(matrix[0], result[0], 0.0000001f);
	EXPECT_NEAR(matrix[1], result[1], 0.0000001f);
	EXPECT_NEAR(matrix[2], result[2], 0.0000001f);

	EXPECT_NEAR(matrix[3], result[3], 0.0000001f);
	EXPECT_NEAR(matrix[4], result[4], 0.0000001f);
	EXPECT_NEAR(matrix[5], result[5], 0.0000001f);

	EXPECT_NEAR(matrix[6], result[6], 0.0000001f);
	EXPECT_NEAR(matrix[7], result[7], 0.0000001f);
	EXPECT_NEAR(matrix[8], result[8], 0.0000001f);

	quat = fromEuler(0.0f, 90.0f, 0.0f);

	matrix.set(0, 0, 1,
		       0, 1, 0,
		       -1, 0, 0);

	result = quat.toMatrix3x3();

	EXPECT_NEAR(matrix[0], result[0], 0.0000001f);
	EXPECT_NEAR(matrix[1], result[1], 0.0000001f);
	EXPECT_NEAR(matrix[2], result[2], 0.0000001f);

	EXPECT_NEAR(matrix[3], result[3], 0.0000001f);
	EXPECT_NEAR(matrix[4], result[4], 0.0000001f);
	EXPECT_NEAR(matrix[5], result[5], 0.0000001f);

	EXPECT_NEAR(matrix[6], result[6], 0.0000001f);
	EXPECT_NEAR(matrix[7], result[7], 0.0000001f);
	EXPECT_NEAR(matrix[8], result[8], 0.0000001f);
}
