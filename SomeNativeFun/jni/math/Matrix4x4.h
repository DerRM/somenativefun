/*
 * Matrix4x4.h
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#ifndef MATRIX4X4_H_
#define MATRIX4X4_H_

#include <ostream>

namespace chs
{

struct Vector4;

struct Matrix4x4
{
	float m[16];

	Matrix4x4();
	Matrix4x4(float m00, float m01, float m02, float m03,
			  float m10, float m11, float m12, float m13,
			  float m20, float m21, float m22, float m23,
			  float m30, float m31, float m32, float m33);
	Matrix4x4(float (&array)[16]);
	virtual ~Matrix4x4();

	void set(float m00, float m01, float m02, float m03,
			 float m10, float m11, float m12, float m13,
			 float m20, float m21, float m22, float m23,
			 float m30, float m31, float m32, float m33);
	Vector4 getRow(int row);
	void setRow(int row, const Vector4& vec);
	Vector4 getColumn(int col);
	void setColumn(int col, const Vector4& vec);
	Matrix4x4 inverse();
	Matrix4x4 transpose();
	float determinant();
	float operator [](int index) const;
	float& operator [](int index);

	static Matrix4x4 identity();
	static Matrix4x4 zero();
	static Matrix4x4 ones();

private:
	float getCofactor(float m0, float m1, float m2,
					  float m3, float m4, float m5,
					  float m6, float m7, float m8);
};

Matrix4x4 operator *(const Matrix4x4& m0, const Matrix4x4& m1);
Vector4 operator *(const Matrix4x4& matrix, const Vector4& vec);
std::ostream& operator <<(std::ostream& strm, const Matrix4x4& m);
} /* namespace chs */

#endif /* MATRIX4X4_H_ */
