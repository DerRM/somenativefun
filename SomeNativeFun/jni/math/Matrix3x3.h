/*
 * Matrix3x3.h
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#ifndef MATRIX3X3_H_
#define MATRIX3X3_H_

#include <ostream>

namespace chs
{

struct Vector3;

struct Matrix3x3
{
	float m[9];

	Matrix3x3();
	Matrix3x3(float m00, float m01, float m02,
			  float m10, float m11, float m12,
			  float m20, float m21, float m22);
	Matrix3x3(float (&array)[9]);
	virtual ~Matrix3x3();

	void set(float m00, float m01, float m02,
			 float m10, float m11, float m12,
			 float m20, float m21, float m22);
	Vector3 getRow(int row);
	void setRow(int row, const Vector3& vec);
	Vector3 getColumn(int col);
	void setColumn(int col, const Vector3& vec);
	float getValue(int row, int col);
	void setValue(int row, int col, float value);
	Matrix3x3 inverse();
	Matrix3x3 transpose();
	float determinant();
	float operator [](int index) const;
	float& operator [](int index);

	static Matrix3x3 identity();
	static Matrix3x3 zero();
	static Matrix3x3 ones();
};

Matrix3x3 operator *(const Matrix3x3& m0, const Matrix3x3& m1);
Vector3 operator *(const Matrix3x3& matrix, const Vector3& vec);
std::ostream& operator <<(std::ostream& strm, const Matrix3x3& m);

} /* namespace chs */

#endif /* MATRIX3X3_H_ */
