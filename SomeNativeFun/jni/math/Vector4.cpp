/*
 * Vector4.cpp
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#include "math/Vector4.h"

namespace chs
{

Vector4::Vector4()
: x(0.0f), y(0.0f), z(0.0f), w(0.0f)
{
}

Vector4::Vector4(float x, float y, float z, float w)
: x(x), y(y), z(z), w(w)
{
}

Vector4 Vector4::normalize() const
{
	return *this / magnitude();
}

float Vector4::magnitude() const
{
	return std::sqrt(magnitude_sqr());
}

float Vector4::magnitude_sqr() const
{
	return x * x + y * y + z * z + w * w;
}

const Vector4 Vector4::operator /(float value) const
{
	return Vector4(x / value, y / value, z / value, w / value);
}

Vector4& Vector4::operator /=(float value)
{
	x /= value;
	y /= value;
	z /= value;
	w /= value;

	return *this;
}

const Vector4 Vector4::operator -() const
{
	return Vector4(-x, -y, -z, -w);
}

const Vector4 Vector4::operator *(float scale) const
{
	return Vector4(x * scale, y * scale, z * scale, w * scale);
}

Vector4& Vector4::operator *=(float scale)
{
	x *= scale;
	y *= scale;
	z *= scale;
	w *= scale;

	return *this;
}

const float Vector4::dot(const Vector4& v) const
{
	return x * v.x + y * v.y + z * v.z + w * v.w;
}

const Vector4 Vector4::modulate(const Vector4& v) const
{
	return Vector4(x * v.x, y * v.y, z * v.z, w * v.w);
}

Vector4& Vector4::operator +=(const Vector4& vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;
	w += vec.w;

	return *this;
}

Vector4& Vector4::operator -=(const Vector4& vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
	w -= vec.w;

	return *this;
}

Vector4 Vector4::zero()
{
	return Vector4();
}

Vector4 Vector4::ones()
{
	return Vector4(1.0f, 1.0f, 1.0f, 1.0f);
}

const Vector4 operator +(const Vector4& v0, const Vector4& v1)
{
	return Vector4(v0.x + v1.x, v0.y + v1.y, v0.z + v1.z, v0.w + v1.w);
}

const Vector4 operator -(const Vector4& v0, const Vector4& v1)
{
	return Vector4(v0.x - v1.x, v0.y - v1.y, v0.z - v1.z, v0.w - v1.w);
}

std::ostream& operator <<(std::ostream& strm, const Vector4& v)
{
	strm << v.x << " " << v.y << " " << v.z << " " << v.w;

	return strm;
}

} /* namespace chs */
