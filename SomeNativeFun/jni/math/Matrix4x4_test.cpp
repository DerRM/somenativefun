/*
 * Matrix4x4_test.cpp
 *
 *  Created on: 27.07.2014
 *      Author: DerRM
 */

#include <gtest/gtest.h>
#include <math/Matrix4x4.h>
#include <math/Vector4.h>

using namespace chs;

TEST(Matrix4x4, MatrixTimesVector)
{
	Matrix4x4 matrix(1, 0, 2, 0,
				     0, 3, 0, 4,
				     0, 0, 5, 0,
				     6, 0, 0, 7);

	Vector4 vec(2, 5, 1, 8);
	Vector4 expected(4, 47, 5, 68);
	Vector4 result = matrix * vec;

	EXPECT_FLOAT_EQ(expected.x, result.x);
	EXPECT_FLOAT_EQ(expected.y, result.y);
	EXPECT_FLOAT_EQ(expected.z, result.z);
	EXPECT_FLOAT_EQ(expected.w, result.w);
}

TEST(Matrix4x4, MatrixTimesMatrix)
{
	Matrix4x4 matrix0(1, 2, 3, 4,
			  	  	  5, 6, 7, 8,
			  	  	  1, 2, 3, 4,
			  	  	  5, 6, 7, 8);
	Matrix4x4 matrix1(1, 2, 3, 4,
			  	  	  5, 6, 7, 8,
			  	  	  1, 2, 3, 4,
			  	  	  5, 6, 7, 8);
	Matrix4x4 expected(34, 44, 54, 64,
					 82, 108, 134, 160,
					 34, 44, 54, 64,
					 82, 108, 134, 160);

	Matrix4x4 result = matrix0 * matrix1;

	EXPECT_FLOAT_EQ(expected[0], result[0]);
	EXPECT_FLOAT_EQ(expected[4], result[4]);
	EXPECT_FLOAT_EQ(expected[8], result[8]);
	EXPECT_FLOAT_EQ(expected[12], result[12]);

	EXPECT_FLOAT_EQ(expected[1], result[1]);
	EXPECT_FLOAT_EQ(expected[5], result[5]);
	EXPECT_FLOAT_EQ(expected[9], result[9]);
	EXPECT_FLOAT_EQ(expected[13], result[13]);

	EXPECT_FLOAT_EQ(expected[2], result[2]);
	EXPECT_FLOAT_EQ(expected[6], result[6]);
	EXPECT_FLOAT_EQ(expected[10], result[10]);
	EXPECT_FLOAT_EQ(expected[14], result[14]);

	EXPECT_FLOAT_EQ(expected[3], result[3]);
	EXPECT_FLOAT_EQ(expected[7], result[7]);
	EXPECT_FLOAT_EQ(expected[11], result[11]);
	EXPECT_FLOAT_EQ(expected[15], result[15]);

//	std::cout << expected << "\n";
//	std::cout << result << "\n";
}

TEST(Matrix4x4, Determinant)
{
	Matrix4x4 matrix(5, 0, 3, -1,
			         3, 0, 0, 4,
			         -1, 2, 4, -2,
			         1, 0, 0, 5);
	float expected = 66;
	float result = matrix.determinant();

	EXPECT_FLOAT_EQ(expected, result);
}
