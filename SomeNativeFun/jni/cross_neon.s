@ Cross product with neon
.text
.global cross_neon

pResult .req r0
pVec1 	.req r1
pVec2 	.req r2

cross_neon:

	@vld1.32 	{q0}, [pVec1] 	@ q0 = (pVec1.x, pVec1.y, pVec1.z, pVec1.w)
	@vld1.32	{q1}, [pVec2]	@ q1 = (pVec2.x, pVec2.y, pVec2.z, pVec2.w)

	vtrn.32 	d2, d3			@ q1 = (pVec2.x, pVec2.z, pVec2.y, pVec2.w)
	vrev64.32 	d0, d0			@ q0 = (pVec1.y, pVec1.x, pVec1.z, pVec1.w)
	vrev64.32	d2, d2			@ q1 = (pVec2.z, pVec2.x, pVec2.y, pVec2.w)
	vtrn.32		d0, d1			@ q0 = (pVec1.y, pVec1.z, pVec1.x, pVec1.w)

	vmul.f32	q2, q0, q1		@ q2 = (pVec1.y * pVec2.z, pVec1.z * pVec2.x, pVec1.x * pVec2.y, pVec1.w * pVec2.w)

	vtrn.32		d2, d3			@ q1 = (pVec2.z, pVec2.y, pVec2.x, pVec2.w)
	vrev64.32	d0, d0			@ q0 = (pVec1.z, pVec1.y, pVec1.x, pVec1.w)
	vrev64.32	d2, d2			@ q1 = (pVec2.y, pVec2.z, pVec2.x, pVec2.w)
	vtrn.32		d0, d1			@ q0 = (pVec1.z, pVec1.x, pVec1.y, pVec1.w)

	vmls.f32	q2, q0, q1		@ q2 = (pVec1.y * pVec2.z - pVec1.z * pVec2.y, pVec1.z * pVec2.x - pVec1.x * pVec2.z,
								@ 		pVec1.x * pVec2.y - pVec1.y * pVec2.x, pVec1.w * pVec2.w - pVec1.w * pVec2.w)

	vmov.f32	q0, q2

	@vst1.32	{q2}, [pResult]
	@vadd.f32 	q0, q0, q1

	bx 			lr
