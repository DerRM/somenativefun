#version 100

uniform sampler2D u_TextureUnit;
varying mediump vec2 v_TextureCoordinates;

void main()
{
	gl_FragColor = texture2D(u_TextureUnit, v_TextureCoordinates);
}
