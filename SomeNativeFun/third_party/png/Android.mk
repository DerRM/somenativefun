LOCAL_PATH := $(call my-dir)/libpng

include $(CLEAR_VARS)

LS_C=$(subst $(1)/,,$(wildcard $(1)/$(2)*.c))

LOCAL_MODULE := png
LOCAL_SRC_FILES := $(call LS_C,$(LOCAL_PATH),src/)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_LDLIBS := -lz

include $(BUILD_STATIC_LIBRARY)

